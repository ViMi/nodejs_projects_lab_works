//Include Modules
const express = require('express');
const hbs = require('hbs');
const router = require("./src/routers/weatherRouter.js");
//!----


//Create a work variables
let app = express();


//Init Static Files
app.use("/css/bootstrap.css",express.static(__dirname+"/node_modules/bootstrap/dist/css/bootstrap.css"));
app.use("/js/bootstrap.js",express.static(__dirname + "/node_modules/bootstrap/dist/js/bootstrap.js"));
app.use("/res/img/placeholder.jpg", express.static(__dirname + "/public/img/placeholder.jpg"));

//Init Template Engine
hbs.registerPartials(__dirname + '/views/partials');
app.set('views engine', 'hbs');


//App Codes
app.use(router);
app.listen(8080, () => {console.log("Example app listening on port 8080");});

