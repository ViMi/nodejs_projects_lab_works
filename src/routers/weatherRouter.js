//-------------Load Modules
const express = require('express');
const hbs = require('hbs');
const fs = require('fs');
const weather = require('openweather-apis');
//!---------------


//--------------Init work Variables
const appTitle = "Weather App";
const rootWay = "/weather";

const stdCity = "Kiev";



const apiKey = 'a4b3be60f815a75a9d8aa3340b16e929';
let cities = JSON.parse(fs.readFileSync('public/json/city_name.json', 'utf8'));

const mainPageRender =
        res => res.render('weather.hbs', {cities, rootWay, appTitle, stdCity});
const weatherRender =
    (res, weatherInfo) => res.render("weather_cities.hbs",{weatherInfo, cities, appTitle, rootWay, stdCity});

//----
const getCityParam = (req) =>
{
    if(!req.query.city)
        return (req.params.city) ? req.params.city : stdCity;
    else
        return req.query.city;
};


const findCordByCity =
        city => cities.some(element => element.name === city)
            ? cities.find(element => element.name === city) : {};
//----

const router = express.Router();
//!------------


//Configure API
weather.setLang("ua");
weather.setUnits('metric');
weather.setAPPID(apiKey);
hbs.registerPartials(__dirname + '/views/partials');

//!---------------



//Main Logic
router.get('/', (req, res) => mainPageRender(res));
router.get('/weather', (req, res) => mainPageRender(res));
router.get('/weather(/:city)?', (req, res) =>
{
    let {lon=null, lat=null} = findCordByCity(getCityParam(req));
    weather.setCoordinate(lat,lon);
    weather.getAllWeather((err,weatherInfo) => { console.log(weatherInfo); weatherRender(res,weatherInfo);});
});
//!--------

module.exports = router;